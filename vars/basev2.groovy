def call(body) {
    def pipelineParams= [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body()

    //
    // This pipeline should not be used on Jenkins production.
    //
    pipeline {

        triggers {
            cron('H H H * *')
        }

        environment {
            CI_AGENT_MAVEN_JAVA = "${body.ciAgentMavenJava}"
            NEXUSIQ_URL = 'https://nexus-iq.bnc.ca'
            TEST_REPORT_NAME = "${body.testCredentials}"
            DEPLOY_JAR = "${body.deployJar}"

            DEPENDENCY_CHECK = 0
            MUTATION = 0
        }

        options {
            disableConcurrentBuilds()
            ansiColor('xterm')
            buildDiscarder(logRotator(numToKeepStr: '10'))
            timestamps()
        }

        agent { label 'shared-agent-jdk-11' }

        stages {
            stage ('Setup') {
                parallel {
                    stage ('Initialize') {
                        steps {
                            script {
                                configFileProvider([configFile(fileId: 'context', targetLocation: 'context.json')]) {
                                    contextConfig = readJSON file: 'context.json'
                                    toolStackId = contextConfig.tool_stack.active.id
                                }

                                withCredentials([usernameColonPassword(credentialsId: "${toolStackId}.source-code-management", variable: 'TOKEN')]) {
                                    library( identifier: 'paap-jenkins-lib@latest', retriever:
                                              modernSCM([$class: 'GitSCMSource', remote: "https://${env.TOKEN}@git.bnc.ca/scm/app5703/paap-jenkins-lib.git"])
                                    )
                                }

                                toolStack = paap.getActiveToolStack()
                                applicationId = paap.getApplicationId()
                                environmentName = paap.getEnvironment()
                                deploymentStage = paap.getDeploymentStage()

                                artifactManagement = toolStack.getToolInstance('artifact-management')
                                codeQualityManagement = toolStack.getToolInstance('code-quality-management')
                                containerManagementPlatform = toolStack.getToolInstance('container-management-platform')
                                secretManagement = toolStack.getToolInstance('secret-management')
                                sourceCodeManagement = toolStack.getToolInstance('source-code-management')

                                env.NEXUS_PROD_LIB_URL = "${artifactManagement.apiURL}/repository/${applicationId}-mvn-production-local"

                            }
                        }
                    }

                    stage ('Validate') {
                       when {
                            expression { isPRToReleaseOrMaster() }
                        }
                        steps {
                            script {
                                if (env.CHANGE_TARGET.equals('release') && !env.CHANGE_BRANCH.equals('develop')) {
                                    echo 'In order to merge to Release you must start from Develop'
                                    currentBuild.result = 'UNSTABLE'
                                    return
                                }
                                if (env.CHANGE_TARGET.equals('master') && !env.CHANGE_BRANCH.equals('release')) {
                                    echo 'In order to merge to Master you must start from Release'
                                    currentBuild.result = 'UNSTABLE'
                                    return
                                }
                            }
                        }
                    }
                }
            }

            stage ('Setting Variables') {
                steps {
                    script {
                        artifactCredentials = artifactManagement.getServiceAccountCredentials()
                        env.NEXUS_USERNAME = artifactCredentials.username
                        env.NEXUS_PASSWORD = artifactCredentials.password
                        env.NEXUS_URL = artifactManagement.apiURL
                        env.NEXUS_APPID = applicationId

                        serverName = "${artifactManagement.apiURL}"
                        netServer = serverName.replace('https://', '')
                        paapUtils.shWithNoTrace("echo machine ${netServer} login ${env.NEXUS_USERNAME} password ${env.NEXUS_PASSWORD} > .netrc")

                        dockerRegistryDev = artifactManagement.getDockerRegistry('development')
                        dockerRegistryStaging = artifactManagement.getDockerRegistry('staging')
                        dockerRegistryProd = artifactManagement.getDockerRegistry('production')

                        componentVersion = sh(script: 'mvn help:evaluate -Dexpression=project.version -q -DforceStdout -s settings.xml', returnStdout: true).trim()
                        componentName = sh(script: 'mvn help:evaluate -Dexpression=project.name -q -DforceStdout -s settings.xml', returnStdout: true).trim()
                        componentArtifactId = sh(script: 'mvn help:evaluate -Dexpression=project.artifactId -q -DforceStdout -s settings.xml', returnStdout: true).trim()
                        componentGroupId = sh(script: 'mvn help:evaluate -Dexpression=project.groupId -q -DforceStdout -s settings.xml', returnStdout: true).trim()

                        jarFile = "${componentArtifactId}-${componentVersion}.jar"

                        servicesCredentials = secretManagement.getCredentials('development/services-credentials')

                        dockerImageName = artifactManagement.getDockerImageName("${componentArtifactId}", "${componentVersion}", "${deploymentStage}")

                        echo "componentName = ${componentName}"
                        echo "componentArtifactId = ${componentArtifactId}"
                        echo "componentVersion = ${componentVersion}"
                        echo "jarFile = ${jarFile}"
                    }
                }
            }

            stage ('Building Application') {
                when {
                    expression { doWeBuild() }
                }
                steps {
                    script {
                        currentBuild.description = "Building ${componentVersion}"

                        sh 'mvn clean package -s settings.xml'

                        stash name: 'stashBuildApplication', includes: 'target/*'
                        stash name: 'stashSurefire', includes: 'target/surefire-reports/*'

                        archiveArtifacts(allowEmptyArchive: false,
                                         artifacts: "target/${jarFile}",
                                         onlyIfSuccessful: false)
                        junit(testResults: 'target/surefire-reports/*.xml', allowEmptyResults: true)
                        publishHTML([allowMissing: false,
                                     alwaysLinkToLastBuild: false,
                                     keepAll: true,
                                     reportDir: 'target/jacoco-report/',
                                     reportFiles: 'index.html',
                                     reportName: 'Code coverage',
                                     reportTitles: 'Code coverage report.'])
                    }
                }
            }

            stage ('Building Docker Image') {
                when {
                    expression { doWeBuild() && isFileExist('Dockerfile') }
                }
                steps {
                    script {
                        if (!fileExists("./target/${jarFile}")) {
                            unstash 'stashBuildApplication'
                        }
                        sh "docker build --build-arg argVersion=${componentVersion} --build-arg argComponentName=${componentArtifactId} -t ${dockerImageName} --no-cache ."
                    }
                }
            }

            stage ('Testing') {
                when {
                    expression { doWeBuild() }
                }
                parallel {
                    stage ('Mutation Test') {
                        steps {
                            script {
                                docker.withRegistry("https://${dockerRegistryProd}", "${toolStackId}.artifact-management") {
                                    docker.image(CI_AGENT_MAVEN_JAVA).inside('-v /home/jenkins/.m2:/home/ci/.m2') {

                                        unstash 'stashBuildApplication'

                                        sh 'mvn test -s settings.xml -Pmutation'

                                        pitReportFolder = sh(script: 'ls target/pit-reports/ | sort | head -1', returnStdout: true).trim()
                                        sh "mv target/pit-reports/${pitReportFolder}/* target/pit-reports"
                                        sh "rmdir target/pit-reports/${pitReportFolder}"

                                        stash name: 'stashTargetMutation', includes: 'target/pit-reports/*'

                                        publishHTML([allowMissing: false,
                                                    alwaysLinkToLastBuild: true,
                                                    keepAll: true,
                                                    reportDir: 'target/pit-reports',
                                                    reportFiles: 'index.html',
                                                    reportName: 'Mutation test coverage',
                                                    reportTitles: 'Mutation test coverage report.'])
                                        env.MUTATION = 1
                                    }
                                }
                            }
                        }
                    }

                    stage ('Dependency Check') {
                        steps {
                            script {
                                 docker.withRegistry("https://${dockerRegistryProd}", "${toolStackId}.artifact-management") {
                                    docker.image(CI_AGENT_MAVEN_JAVA).inside('-v /home/jenkins/.m2:/home/ci/.m2') {
     
                                        unstash 'stashBuildApplication'

                                        sh 'mvn verify -s settings.xml -Powasp'
     
                                        stash name: 'stashTargetDependencyCheck', includes: 'target/owasp-report/*'

                                        publishHTML([allowMissing: false,
                                                    alwaysLinkToLastBuild: false,
                                                    keepAll: true,
                                                    reportDir: 'target/owasp-report',
                                                    reportFiles: 'dependency-check-report.html',
                                                    reportName: 'Dependency check',
                                                    reportTitles: 'Dependency check report.'])
                                        env.DEPENDENCY_CHECK = 1
                                    }
                                }
                            }
                        }
                    }

                    stage ('Integration Tests') {
                        when {
                            expression { doWeBuild() && isFileExist('scripts/JenkinsTestRunner.sh') }
                        }
                        steps {
                            script {     
                                docker.withRegistry("https://${dockerRegistryDev}", "${toolStackId}.artifact-management") {

                                    if ( "${body.testCredentials}" == 'null' ) {
                                        itestCredentials = secretManagement.getCredentials('development/integration-tests/config')
                                    }
                                    else {
                                        itestCredentials = secretManagement.getCredentials("development/integration-tests/${body.testCredentials}")
                                    }
                                    itestCredentials.each { key, value ->
                                        env."${key}" = "${value}"
                                    }

                                    containerCredentials = containerManagementPlatform.getServiceAccountCredentials()
                                    env.OPENSHIFT_TOKEN = containerCredentials.access_token
                                    env.OPENSHIFT_SERVER = "${containerManagementPlatform.apiURL}"

                                    sh "scripts/JenkinsTestRunner.sh ${componentVersion} development"

                                    if (fileExists('reports/itests') || fileExists('reports/end2end')) { 
                                        stash name: 'stashTestsReport', includes: 'reports/**/*'
                                    }

                                    if (fileExists('reports/itests')) { 
                                        publishHTML([allowMissing: false,
                                                    alwaysLinkToLastBuild: false,
                                                    keepAll: true,
                                                    reportDir: 'reports/itests',
                                                    reportFiles: 'index.html',
                                                    reportName: 'Component tests',
                                                    reportTitles: 'Component tests report.'])
                                    }

                                    if (fileExists('reports/end2end')) { 
                                        publishHTML([allowMissing: false,
                                                    alwaysLinkToLastBuild: false,
                                                    keepAll: true,
                                                    reportDir: 'reports/end2end',
                                                    reportFiles: 'index.html',
                                                    reportName: 'end to end tests',
                                                    reportTitles: 'end to end tests report.'])
                                    }
                                }
                            }
                        }
                    }
                }
            }

            stage ('Code Quality') {
                when {
                    expression { doWeBuild() }
                }
                parallel {
                    stage ('Sonar') {
                        stages {
                            stage ('Checking Sonar Project') {
                                steps {
                                    script {

                                        codeQualityCredentials = codeQualityManagement.getServiceAccountCredentials()

                                        serverName = "${codeQualityManagement.apiURL}"
                                        netServer = serverName.replace('https://', '')
                                        paapUtils.shWithNoTrace("echo machine ${netServer} login ${codeQualityCredentials.username} password ${codeQualityCredentials.password} >> .netrc")

                                        sonarProjectKey = "APP${applicationId}.${componentGroupId}:${componentArtifactId}"

                                        sonarStatus = sh(script: "curl -s --netrc-file .netrc -X GET '${codeQualityManagement.apiURL}/api/qualitygates/project_status?projectKey=${sonarProjectKey}&branch=master' | jq -r '.projectStatus.status'", returnStdout: true).trim()
                                        
                                        echo "Checking if the project already exist: ${sonarStatus}"
                                    }
                                }
                            }    

                            stage ('SonarQube') {
                                steps {
                                    script {
                                        if ( env.MUTATION == 1 ) {
                                            unstash 'stashTargetMutation'
                                        }
                                        if ( env.DEPENDENCY_CHECK == 1 ) {
                                            unstash 'stashTargetDependencyCheck'
                                        }

                                        if ( sonarStatus == 'null' ) {
                                            attributes = ''
                                            echo 'Project does not exist in Sonar.'
                                        } else {
                                            attributes = getSonarBranchParam()
                                        }

                                        unstash 'stashSurefire'

                                        sonarScm = "${sourceCodeManagement.apiURL}/projects/APP${applicationId}/repos/${componentArtifactId}"
                                        sh "mvn sonar:sonar -Dsonar.host.url=${codeQualityManagement.apiURL} ${attributes} -Dsonar.projectKey=${sonarProjectKey} -Dsonar.links.homepage=${body.sonarHomepage} -Dsonar.links.scm=${sonarScm} -s settings.xml"
                                    }
                                }
                            }

                            stage ('Sonar Quality Gate') {
                                steps {
                                    script {
                                        attributes = qualityGateBranch()
                                        SonarQualityGateStatus = sh(script: "curl -s --netrc-file .netrc -X GET '${codeQualityManagement.apiURL}/api/qualitygates/project_status?projectKey=${sonarProjectKey}${attributes}' | jq -r '.projectStatus.status'", returnStdout: true).trim()
                                        echo "Gate status = ${SonarQualityGateStatus}"
                                        if ( SonarQualityGateStatus == 'ERROR' ) {
                                            error('Quality Gate for Sonar is failing.')
                                        }
                                    }
                                }
                            }

                            stage ('Setting Qualities') {
                                 steps {
                                    script {
                                        sh "curl -s --netrc-file .netrc -X POST -d language=java -d projectKey=${sonarProjectKey} -d \"qualityProfile=Sonar way 5.8 BNC\" ${codeQualityManagement.apiURL}/api/qualityprofiles/add_project"
                                        gateId = sh(script: "curl -s --netrc-file .netrc -X GET ${codeQualityManagement.apiURL}/api/qualitygates/list | jq -r '.qualitygates[]| select (.name==\"Sonar way\")|.id'", returnStdout: true).trim()
                                        echo "Gate Id = ${gateId}"
                                        sh "curl -s --netrc-file .netrc -X POST -d gateId=${gateId} -d projectKey=${sonarProjectKey} ${codeQualityManagement.apiURL}/api/qualitygates/select"
                                    }
                                }
                            }
                        }
                    }

                    stage ('NexusIQ') {
                        stages {
                            stage ('Setting NexusIQ Project') {
                                steps {
                                    script {
                                        nexusIqCredentials = servicesCredentials.nexusIq

                                        serverName = "${NEXUSIQ_URL}"
                                        netServer = serverName.replace('https://', '')
                                        paapUtils.shWithNoTrace("echo machine ${netServer} login ${nexusIqCredentials.username} password \"${nexusIqCredentials.password}\" >> .netrc")

                                        organizationId = sh(script: "curl -s --netrc-file .netrc -X GET '${NEXUSIQ_URL}/api/v2/organizations' | jq -r '.organizations[] | select(.name == \"APP${applicationId}-${body.domainName}\")|.id'", returnStdout: true).trim()
                                        echo "Organization Id = ${organizationId}"
                                        sh "curl -s --netrc-file .netrc -X POST -H 'Content-Type: application/json' -d '{\"publicId\":\"${componentArtifactId}\", \"name\": \"${componentArtifactId}\",\"organizationId\": \"${organizationId}\"}' '${NEXUSIQ_URL}/api/v2/applications'"
                                    }
                                }
                            }

                            stage ('Scanning Project') {
                                steps {
                                    script {
                                        paapUtils.shWithNoTrace("nexus-iq -i ${componentArtifactId} -s ${NEXUSIQ_URL} -a \"${nexusIqCredentials.username}\":\"${nexusIqCredentials.password}\" target/*.jar")
                                    }
                                }
                            }

                            stage ('NexusIQ Vulnerabilities Gate') {
                                steps {
                                    script {
                                        nexusIQAppId = sh(script: "curl -s --netrc-file .netrc -X GET '${NEXUSIQ_URL}/api/v2/applications' | jq -r '.applications[] | select(.name == \"${componentArtifactId}\")|.id'", returnStdout: true).trim()
                                        echo "AppId = ${nexusIQAppId}"
                                        nexusIQReport = sh(script: "curl -s --netrc-file .netrc -X GET '${NEXUSIQ_URL}/api/v2/reports/applications/${nexusIQAppId}' | jq -r '.[].reportDataUrl'", returnStdout: true).trim()
                                        echo "Report = ${nexusIQReport}"
                                        nexusIQReportPolicy = nexusIQReport.replace('/raw', '/policy')
                                        echo "Policy = ${nexusIQReportPolicy}"
                                        nexusIQVulnerabilities = sh(script: "curl -s --netrc-file .netrc -X GET '${NEXUSIQ_URL}/${nexusIQReportPolicy}' | jq -r '.components[] | .violations[] | select(.policyThreatLevel>=7 and .waived==false)' | wc -l", returnStdout: true).trim()
                                        echo "Number of Vulnerabilities = ${nexusIQVulnerabilities}"
                                        if ( "${nexusIQVulnerabilities}" > 0 ) {
                                            error('Vulnerabilities Gate for NexusIQ is too high.')
                                        }
                                    }
                                }
                            }
                        }
                    }

                    stage ('Veracode') {
                        when {
                            branch 'develop-test'
                        }
                        steps {
                            script {
                                commitShortId = sh(script: 'git rev-parse --short HEAD', returnStdout: true).trim()
                                veracodeVersion = "${componentVersion}-${env.BUILD_NUMBER}-${commitShortId}"
                                veracodeCredentials = servicesCredentials.veracode
                                paapUtils.shWithNoTrace("veracode -action uploadandscan -vid \"${veracodeCredentials.id}\" -vkey \"${veracodeCredentials.key}\" -appname 'APP${applicationId}-${body.domainName}' -createprofile true -criticality VeryHigh -sandboxname ${componentArtifactId} -createsandbox true -version ${veracodeVersion} -filepath target/*.jar")
                            }
                        }
                    }

                    stage ('Generating Dependency List') {
                        steps {
                            echo 'Generating the dependency list report'
                            script {
                                docker.withRegistry("https://${dockerRegistryDev}", "${toolStackId}.artifact-management") {
                                    nexusAgent = '5712/nexus-jenkins-agent:latest'
                                    docker.image(nexusAgent).inside() {
                                        sh "curl -s --netrc-file .netrc ${artifactManagement.apiURL}/repository/cfg-raw/scm/tp.txt -o tp.txt"
                                        sh "cm-generate-dependencies -project='APP${applicationId}_${componentArtifactId}' -html=output.html -tpfile=tp.txt > 'APP${applicationId}-${componentArtifactId}_${componentVersion}.cfg'"

                                        stash name: 'stashCfgFile', includes: "APP${applicationId}-${componentArtifactId}_${componentVersion}.cfg"
                                        stash name: 'stashOutputHtmlFile', includes: 'output.html'
                                    }
                                }
                            }
                        }
                    }

                    stage ('Generating Swagger Apis') {
                        when {
                            expression { isFileExist('src/main/resources/openapi-config.yaml') && isFileExist('./scripts/SwaggerSpecGenerator.sh') }
                        }
                        steps {
                            script {
                                if (!fileExists("./target/${jarFile}")) {
                                    unstash 'stashBuildApplication'
                                }
                                domainName = sh(script: "echo ${body.domainName} | sed s'/-/ /'", returnStdout: true).trim()
                                echo "domainName = ${domainName}"
                                echo 'Generating swagger documentation'
                                sh "./scripts/SwaggerSpecGenerator.sh \"${domainName}\" \"${componentName}\" \"${componentArtifactId}\""

                                stash name: 'stashOpenApiFiles', includes: 'target/openApiFiles/*'
                            }
                        }
                    }
                }
            }

            stage ('Pushing') {
                when {
                    branch 'develop'
                }
                parallel {
                    stage ('Dependency Config') {
                        steps {
                            echo 'Publishing the Dependency List'
                            script {
                                unstash 'stashCfgFile'
                                sh "curl -s --netrc-file .netrc --upload-file 'APP${applicationId}-${componentArtifactId}_${componentVersion}.cfg' ${artifactManagement.apiURL}/repository/cfg-raw/scm/"
                            }
                        }
                    }

                    stage ('Image') {
                        when {
                            expression { fileExists('Dockerfile') }
                        }
                        steps {
                            script {
                                docker.withRegistry("https://${dockerRegistryDev}", "${toolStackId}.artifact-management") {
                                    sh "docker push ${dockerImageName}"
                                }
                              
                            }
                        }
                    }

                    stage ('Jira Xray') {
                        when {
                            expression { isFileExist('itests/pom.xml') }
                        }
                        steps {
                            script {
                                if (fileExists('scripts/JenkinsTestRunner.sh')) {
                                    unstash 'stashTestsReport'
                                    xrayCredentials = servicesCredentials.xray
                                    env.XRAY_USERNAME = xrayCredentials.username
                                    env.XRAY_PASSWORD = xrayCredentials.password

                                    if ( "${TEST_REPORT_NAME}" == 'null' ) {
                                        TEST_REPORT_NAME = 'resultats'
                                    }

                                    sh 'mkdir itests/reportTestNg'
                                    if (fileExists('reports/itests/testng-results.xml')) { 
                                        sh "mv reports/itests/testng-results.xml itests/reportTestNg/${componentArtifactId}.xml"
                                    }
                                    if (fileExists('reports/end2end/testng-results.xml')) { 
                                        sh "mv reports/end2end/testng-results.xml itests/reportTestNg/${TEST_REPORT_NAME}-end2end.xml"
                                    }

                                    if (fileExists("itests/reportTestNg/${componentArtifactId}.xml") || fileExists("itests/reportTestNg/${TEST_REPORT_NAME}-end2end.xml")) { 
                                        sh "export CM_USER='test' && export CM_PASSWORD='test' && export CM_URL='http://localhost:8080' && cd ./itests && mvn com.xpandit.xray:xray-maven-plugin:xray -Dxray.revision=${componentVersion} -Dxray.testPlanKey=${xrayCredentials.testPlanKey} -Dxray.surefire.location=reportTestNg -s settings.xml"
                                    }
                                }
                            }
                        }
                    }

                    stage ('Library') {
                        when {
                            expression { !fileExists('Dockerfile') }
                        }
                        steps {
                            script {
                                sh "mvn deploy:deploy-file -DgeneratePom=false -DrepositoryId=${artifactManagement.tool} -Durl=${NEXUS_PROD_LIB_URL} -DpomFile=pom.xml -Dfile=target/${jarFile} -s settings.xml"
                            }
                        }
                    }

                    stage ('OpenApi') {
                        when {
                            expression { isFileExist('src/main/resources/openapi-config.yaml') }
                        }
                        steps{
                            script {
                                if (!fileExists("target/openApiFiles/contractFiles.txt")) {
                                   unstash 'stashOpenApiFiles'
                                }
                                confluenceCredentials = servicesCredentials.confluence
                                if (fileExists('target/openApiFiles/contractFiles.txt')) {
                                    apiFile = readFile 'target/openApiFiles/contractFiles.txt'
                                    apiVersions = apiFile.readLines()
                                    apiVersions.each { apiVersion ->
                                        echo "Publishing ${apiVersion}"
                                        paapUtils.shWithNoTrace("openapi-confluence-content-generator --username=\"${confluenceCredentials.username}\" --password=\"${confluenceCredentials.password}\" --configFile=${apiVersion}")
                                    }    
                                } else {
                                    paapUtils.shWithNoTrace("openapi-confluence-content-generator --username=\"${confluenceCredentials.username}\" --password=\"${confluenceCredentials.password}\" --configFile=src/main/resources/openapi-config.yaml")
                                }
                            }
                        }
                    }
                }
            }

            stage ('Publishing HTMLs') {
                when {
                    expression { doWeBuild() }
                }
                steps {
                    echo 'Publishing the Dependency List'
                    script {
                        unstash 'stashOutputHtmlFile'
                        publishHTML([allowMissing: false,
                                    alwaysLinkToLastBuild: false,
                                    keepAll: true,
                                    reportDir: '.',
                                    reportFiles: 'output.html',
                                    reportName: 'Dependencies List',
                                    reportTitles: 'Dependencies List report.'])
                    }
                }
            }

            stage ('Promotion to') {
                parallel {
                    stage ('Staging') {
                        when {
                            expression { env.BRANCH_NAME.equals('release') && isFileExist('Dockerfile') }
                        }
                        steps {
                            script {
                                currentBuild.description = "Promoting ${componentVersion} to Staging"

                                paapUtils.dockerPromote(
                                    "${componentArtifactId}", "${componentVersion}",
                                    artifactManagement, 'development',
                                    artifactManagement, 'staging'
                                )
                            }
                        }
                    }

                    stage ('Production') {
                        when {
                            expression { env.BRANCH_NAME.equals('master') && isFileExist('Dockerfile') }
                        }
                        steps {
                            script {
                                currentBuild.description = "Promoting ${componentVersion} to Production"

                                paapUtils.dockerPromote(
                                    "${componentArtifactId}", "${componentVersion}",
                                    artifactManagement, 'staging',
                                    artifactManagement, 'production'
                                )
                            }
                        }
                    }
                }
            }

            stage ('Tagging') {
                when {
                    branch 'master'
                }
                steps {
                    echo 'Tagging Git'
                    script {
                        sourceCodeCredentials = sourceCodeManagement.getServiceAccountCredentials()

                        sh "git config --global user.email ${sourceCodeCredentials.username}@bnc.ca"
                        sh "git config --global user.name ${sourceCodeCredentials.username}"
                        sh "git tag -a ${componentVersion} -m 'Tagged by Jenkins'"
                        gitUrl = sh(script: "echo ${sourceCodeManagement.apiURL} | cut -f3 -d'/'", returnStdout: true).trim()
                        paapUtils.shWithNoTrace("git push https://${sourceCodeCredentials.username}:${sourceCodeCredentials.password}@${gitUrl}/scm/app${applicationId}/${componentArtifactId}.git ${componentVersion}")
                    }
                }
            }

            stage ('Deploy') {
                when {
                    expression { (env.BRANCH_NAME.equals('develop') || env.BRANCH_NAME.equals('release')) && (isFileExist('Dockerfile') || "${DEPLOY_JAR}" == true) }
                }
                steps {
                    script {
                        currentBuild.description = "Deploying ${componentVersion} to ${deploymentStage}"
        
                        jobPath = "${applicationId}/${environmentName}/${componentArtifactId}-deployment/develop"
                        build job: "${jobPath}", parameters: [
                            string(name: 'version', value: "${componentVersion}"),
                            string(name: 'deploymentStage', value: "${deploymentStage}"),
                            string(name: 'manualTrigger', value: 'no'),
                        ], propagate: true, wait: true
                    }
                }
            }
        }

        post {
            always {
                sh "rm -rf *"
            }
            success {
                script {
                    notifyTeam('success')
                }
            }
            failure {
                script {
                    notifyTeam('failure')
                }
            }
        }
    }
}

def isPRToReleaseOrMaster() {
    return (
        env.BRANCH_NAME.startsWith('PR-') && (env.CHANGE_TARGET.equals('master') || env.CHANGE_TARGET.equals('release'))
    )
}

def doWeBuild() {
    return (
        !env.BRANCH_NAME.equals('master') 
        && !env.BRANCH_NAME.equals('release') 
        && !isPRToReleaseOrMaster()
    )
}

def isFileExist(String filename) {
    return fileExists(filename);
}

def getSonarBranchParam(){
    if (env.BRANCH_NAME.startsWith('PR-')) {
        return "-Dsonar.pullrequest.key=${env.BRANCH_NAME} -Dsonar.pullrequest.branch=${env.CHANGE_BRANCH} -Dsonar.pullrequest.base=${env.CHANGE_TARGET}"
    }
    if (env.BRANCH_NAME.equals('develop')) {
        return '-Dsonar.branch.name=master'
    }
    return "-Dsonar.branch.name=${env.BRANCH_NAME}"
}

def qualityGateBranch() {
    if (env.BRANCH_NAME.startsWith('PR-')) {
        return "&pullRequest=${env.BRANCH_NAME}"
    }
    if (env.BRANCH_NAME.equals('develop')) {
        return '&branch=master'
    }
    return "&branch=${env.BRANCH_NAME}"
}

def notifyTeam(String buildStatus) {
    if (env.BRANCH_NAME.equals('develop')) {

        teamsHook = servicesCredentials.teamsHook.token

        echo "Sending teams notification to: ${teamsHook}"

        if (buildStatus == 'success') {
            paapUtils.success("${teamsHook}", "${applicationId} ${componentArtifactId} <a href='${env.BUILD_URL}'>build</a>")
        } else {
            paapUtils.failure("${teamsHook}", "${applicationId} ${componentArtifactId} <a href='${env.BUILD_URL}'>build</a>")
        }
    }
}